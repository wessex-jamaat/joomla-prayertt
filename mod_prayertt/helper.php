<?php
class ModPrayerTTHelper
{
	public static function getPrayerTimes($cdate)
	{
		$qdate = date('md', $cdate);
		// get a reference to the database
		$db = JFactory::getDBO();
		
		$query = 'SELECT * FROM `#__prayertt` WHERE Date = '.$qdate.'';
		$db->setQuery($query);
		$times = $db->loadObject();
		$times = ModPrayerTTHelper::toLondonTime($times);
		return $times;
	}

	private static function toLondonTime($row)
	{
        $result = new PrayerDate();
        $result->Imsaak = ModPrayerTTHelper::buildLondonDateStr($row->Date, $row->Imsaak);
        $result->Sunrise = ModPrayerTTHelper::buildLondonDateStr($row->Date, $row->Sunrise);
        $result->Fajr = ModPrayerTTHelper::buildLondonDateStr($row->Date, $row->Fajr);
        $result->Zohr = ModPrayerTTHelper::buildLondonDateStr($row->Date, $row->Zohr);
        $result->Sunset = ModPrayerTTHelper::buildLondonDateStr($row->Date, $row->Sunset);
        $result->Maghrib = ModPrayerTTHelper::buildLondonDateStr($row->Date, $row->Maghrib);

	    return $result;
	}

	private static function buildLondonDateStr($datestr, $timestr)
	{
	    $datetimestr = date('Y') . '-' . substr($datestr,0,2) . '-' . substr($datestr,2,2) . ' ' . $timestr . ':00';
        $dt = new DateTime($datetimestr, new DateTimeZone('UTC'));
        $dt->setTimezone(new DateTimeZone('Europe/London'));
        return $dt->format('H:i');
	}

	public static function getDay($cdate, $params)
	{
		// Get day names
		$daynames = array(	0=>$params->get('sunday'),
							1=>$params->get('monday'),
							2=>$params->get('tuesday'),
							3=>$params->get('wednesday'),
							4=>$params->get('thursday'),
							5=>$params->get('friday'),
							6=>$params->get('saturday'));
		$day = intval(gmdate("w",$cdate));
		return $daynames[$day];
	}
	
	public static function getGregDate($cdate, $params)
	{
		// Get Gregorian month names
		$gmonths = array(	1=>$params->get('january'),
							2=>$params->get('february'),
							3=>$params->get('march'),
							4=>$params->get('april'),
							5=>$params->get('may'),
							6=>$params->get('june'),
							7=>$params->get('july'),
							8=>$params->get('august'),
							9=>$params->get('september'),
							10=>$params->get('october'),
							11=>$params->get('november'),
							12=>$params->get('december'));
		$gregsymbol = $params->get('gregsymbol');

        $config = JFactory::getConfig();
        $tzone = $config->get('offset');
        $dt = new DateTime("now", new DateTimeZone($tzone));
        $dt->setTimestamp($cdate);

		$date = intval($dt->format("j"));
		$month = intval($dt->format("n"));
		$year = intval($dt->format("Y"));

		// Format Gregorian date
		$gregdate = $date . " " . $gmonths[$month] . " " . $year;
		if($gregsymbol != '')
			$gregdate = $gregdate . " " . $gregsymbol;
		return $gregdate;
	}
	
	public static function getHijriDate($cdate, &$params)
	{
		// Get Hijri month names
		$hmonths = array(	1=>$params->get('muharram'),
							2=>$params->get('safar'),
							3=>$params->get('rabi1'),
							4=>$params->get('rabi2'),
							5=>$params->get('jamada1'),
							6=>$params->get('jamada2'),
							7=>$params->get('rajab'),
							8=>$params->get('shaban'),
							9=>$params->get('ramadan'),
							10=>$params->get('shawal'),
							11=>$params->get('zulqada'),
							12=>$params->get('zulhajj'));
		$hijrisymbol = $params->get('hijrisymbol');
							
		$date = intval(gmdate("j",$cdate));
		$month = intval(gmdate("n",$cdate));
		$year = intval(gmdate("Y",$cdate));
		
		$jd = ModPrayerTTHelper::greg2jd($date,$month,$year);
		$hijri = ModPrayerTTHelper::jd2hijri($jd);

		// Calculate hijri date
		$hdate = $hijri[0];
		$hmonth = $hijri[1];
		$hyear = $hijri[2];

		// Format Hijri date
		$hijridate = $hdate . " " . $hmonths[$hmonth] . " " . $hyear;
		if($hijrisymbol != '')
			$hijridate = $hijridate . " " . $hijrisymbol;
			
		return $hijridate;
	}

	static function greg2jd($d, $m, $y) 
	{
		$jd = (1461 * ($y + 4800 + ($m - 14) / 12)) / 4 +
		(367 * ($m - 2 - 12 * (($m - 14) / 12))) / 12 -
		(3 * (($y + 4900 + ($m - 14) / 12) / 100 )) / 4 +
		$d - 32075;
		return $jd;
	}

	static function jd2hijri($jd) 
	{
		$jd = $jd - 1948440 + 10632;
		$n = (int) (($jd - 1) / 10631);
		$jd = $jd - 10631 * $n + 354;
		$j = ((int) ((10985 - $jd) / 5316)) *
		((int) (50 * $jd / 17719)) +
		((int) ($jd / 5670)) *
		((int) (43 * $jd / 15238));
		$jd = $jd - ((int) ((30 - $j) / 15)) *
		((int) ((17719 * $j) / 50)) -
		((int) ($j / 16)) *
		((int) ((15238 * $j) / 43)) + 29;
		$m = (int) (24 * $jd / 709);
		$d = $jd - (int) (709 * $m / 24);
		$y = 30 * $n + $j - 30;
		return array((int)$d, $m, $y);
	}

} //end ModPrayerTTHelper

class PrayerDate
{
    public $Imsaak;
    public $Fajr;
    public $Sunrise;
    public $Zohr;
    public $Sunset;
    public $Maghrib;
}