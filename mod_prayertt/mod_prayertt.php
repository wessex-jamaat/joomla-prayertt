<?php
/**
* @version 1.2.0
* @package Joomla 1.5
* @copyright (C) 2010 Prompt IT Solutions LTD
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
*/

// no direct access
defined('_JEXEC') or die('Direct Access to this location is not allowed.');

// include the helper file
require_once dirname(__FILE__).'/helper.php';

// Get Parameters
$calendar	= $params->get( 'calendar' );
$showday	= intval( $params->get( 'showday', 0 ) );
$showprayer = intval( $params->get( 'showprayertimes', 0) );
$firstdate	= $params->get( 'firstdate' );
$separator	= $params->get( 'separator' );
$textbefore	= $params->get( 'textbefore' );
$textafter	= $params->get( 'textafter' );
$showmore   = $params->get( 'showmore' );

// Display Parameters
$format		= intval( $params->get( 'format', 1 ) );
$manual		= trim( $params->get( 'manual' ) );
$adjust		= intval( $params->get( 'adjust', 0 ) );
$moduleclass_sfx = trim( $params->get( 'moduleclass_sfx' ) );

$today = time();
$tomorrow = $today + (24* 3600);

if ($calendar != 'none') {
	$day = ModPrayerTTHelper::getDay($today,$params);
	$gregorian_date = ModPrayerTTHelper::getGregDate($today, $params);
	$hijri_date = ModPrayerTTHelper::getHijriDate($today+($adjust * 24 * 3600), $params);
}
if ($showprayer) {
	$today_txt = date('d/m', $today);
	$tomorrow_txt = date('d/m', $tomorrow);
	$prayer_times_today = ModPrayerTTHelper::getPrayerTimes($today);
	$prayer_times_tomorrow = ModPrayerTTHelper::getPrayerTimes($tomorrow);
}

if ($showmore == 1) {
    $db  = JFactory::getDBO();
    $defaultRedirect = 'index.php?option=com_prayertt&view=prayertt';
    $db->setQuery('SELECT `id` FROM #__menu WHERE `link` LIKE '. $db->Quote($defaultRedirect) .' LIMIT 1' );
    $itemId = ($db->getErrorNum())? 0 : intval($db->loadResult());
    if($itemId){
        $moreLink = JRequest::getString('return', JRoute::_('index.php?Itemid='.$itemId));
    }else{
        $moreLink = JRequest::getString('return', JRoute::_($defaultRedirect));
    }
}

// include the template for display
require JModuleHelper::getLayoutPath('mod_prayertt');
