<?php defined('_JEXEC') or die('Restricted access'); // no direct access ?> 
<?php if($calendar != 'none') { ?>
	<div class="prayer_tt_date <?php echo $moduleclass_sfx; ?>" style="<?php if ($format == 2) echo strip_tags($manual) ?>">
	<?php echo $textbefore; ?>
	
	<?php echo ($showday)?$day:""; ?>
	<?php 
		if($calendar == 'hijri')
		{
			echo($hijri_date);
		}
		else if($calendar == 'gregorian')
		{
			echo($gregorian_date);
		}
		else if($calendar == 'both')
		{
			if($firstdate=='gregorian')
			{
				echo($gregorian_date);
				echo($separator . " ");
				echo($hijri_date);
			}
			else
			{
				echo($hijri_date);
				echo($separator . " ");
				echo($gregorian_date);
			}
		}
		
		echo $textafter;
	?>
	</div>
<?php } //endif calendar!=none?>

<?php if ($showprayer == 1) { ?>
<style type="text/css">
div.prayer_tt_times table {width:100%; border:0}
div.prayer_tt_times td {text-align:left}
</style>

<div class="prayer_tt_times">
<table>
  <tr>
  	<th><strong>Date</strong></th>
  	<th><em><?php echo $today_txt; ?></em></th>
  	<th><em><?php echo $tomorrow_txt; ?></em></th>
  </tr>
  <tr>
    <td>Imsaak:</td>
    <td><?php echo $prayer_times_today->Imsaak; ?></td>
    <td><?php echo $prayer_times_tomorrow->Imsaak; ?></td>
  </tr>
  <tr>
    <td>Fajr:</td>
    <td><?php echo $prayer_times_today->Fajr; ?></td>
    <td><?php echo $prayer_times_tomorrow->Fajr; ?></td>
  </tr>
  <tr>
    <td>Sunrise:</td>
    <td><?php echo $prayer_times_today->Sunrise; ?></td>
    <td><?php echo $prayer_times_tomorrow->Sunrise; ?></td>
  </tr>
  <tr>
    <td>Zohr:</td>
    <td><?php echo $prayer_times_today->Zohr; ?></td>
    <td><?php echo $prayer_times_tomorrow->Zohr; ?></td>
  </tr>
  <tr>
    <td>Maghrib:</td>
    <td><?php echo $prayer_times_today->Maghrib; ?></td>
    <td><?php echo $prayer_times_tomorrow->Maghrib; ?></td>
  </tr>
</table>
</div>

<?php if (isset($moreLink)) { ?>
    <div class="module-more">
        <a href="<?php echo $moreLink ?>">More</a>
    </div>
<?php } ?>

<?php } ?>