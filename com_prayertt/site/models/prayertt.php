<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

class PrayerttModelPrayertt extends JModelItem
{
	private $months = array(
			'jan'=>'January',
			'feb'=>'February',
			'mar'=>'March',
			'apr'=>'April',
			'may'=>'May',
			'jun'=>'June',
			'jul'=>'July',
			'aug'=>'August',
			'sep'=>'September',
			'oct'=>'October',
			'nov'=>'November',
			'dec'=>'December'
	);
	
	private function getUserSelectedMonth()
	{
		$currMonth = strtolower(date('M', time()));
		$jinput = JFactory::getApplication()->input;
		$key    = $jinput->get('month', $currMonth, 'STRING');
		return $key;		
	}

	public function getAllMonthLabels()
	{
		$key    = $this->getUserSelectedMonth();
		$result = array();
		foreach ($this->months as $monthKey => $monthLabel)
		{
			$result[] = array(
							'label'=>$monthLabel, 
							'active'=> ($monthKey == $key ? 'active': ''),
							'key'=> $monthKey);
		}
		return $result;
	}
	
	
	public function getCurrentMonthLabel()
	{
		$key = $this->getUserSelectedMonth();
		return $this->months[$key];
	}
	
	public function getMonthTable()
	{
		switch ($this->getUserSelectedMonth())
		{
			case 'jan':
				$month = $this->getPrayerTimes('01');
				break;
			case 'feb':
				$month = $this->getPrayerTimes('02');
				break;
			case 'mar':
				$month = $this->getPrayerTimes('03');
				break;
			case 'apr':
				$month = $this->getPrayerTimes('04');
				break;
			case 'may':
				$month = $this->getPrayerTimes('05');
				break;
			case 'jun':
				$month = $this->getPrayerTimes('06');
				break;
			case 'jul':
				$month = $this->getPrayerTimes('07');
				break;
			case 'aug':
				$month = $this->getPrayerTimes('08');
				break;
			case 'sep':
				$month = $this->getPrayerTimes('09');
				break;
			case 'oct':
				$month = $this->getPrayerTimes('10');
				break;
			case 'nov':
				$month = $this->getPrayerTimes('11');
				break;
			default:
			case 'dec':
				$month = $this->getPrayerTimes('12');
				break;
		}
		return $month;
	}
	
	
	private function getPrayerTimes($month)
	{
		$db = JFactory::getDBO();
		$query = 'SELECT * FROM `#__prayertt` WHERE LEFT(Date,2) = '.$month.' ORDER BY DATE ASC';
		$db->setQuery($query);
		$times = $db->loadObjectList();
		$result = array();
		foreach ($times as $t)
		{
		    $result[] = PrayerttModelPrayertt::toLondonTime($t);
		}
		return $result;
	}

    private static function toLondonTime($row)
    {
        $result = new PrayerDateCom();
        $result->Date = substr($row->Date,2,2) . '/' . substr($row->Date,0,2);
        $result->Imsaak = PrayerttModelPrayertt::buildLondonDateStr($row->Date, $row->Imsaak);
        $result->Sunrise = PrayerttModelPrayertt::buildLondonDateStr($row->Date, $row->Sunrise);
        $result->Fajr = PrayerttModelPrayertt::buildLondonDateStr($row->Date, $row->Fajr);
        $result->Zohr = PrayerttModelPrayertt::buildLondonDateStr($row->Date, $row->Zohr);
        $result->Sunset = PrayerttModelPrayertt::buildLondonDateStr($row->Date, $row->Sunset);
        $result->Maghrib = PrayerttModelPrayertt::buildLondonDateStr($row->Date, $row->Maghrib);

        return $result;
    }

    private static function buildLondonDateStr($datestr, $timestr)
    {
        $datetimestr = date('Y') . '-' . substr($datestr,0,2) . '-' . substr($datestr,2,2) . ' ' . $timestr . ':00';
        $dt = new DateTime($datetimestr, new DateTimeZone('UTC'));
        $dt->setTimezone(new DateTimeZone('Europe/London'));
        return $dt->format('H:i');
    }
}


class PrayerDateCom
{
    public $Date;
    public $Imsaak;
    public $Fajr;
    public $Sunrise;
    public $Zohr;
    public $Sunset;
    public $Maghrib;
}