<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

class PrayerttViewPrayertt extends JViewLegacy
{
	function display($tpl = null)
	{
		// Assign data to the view
		$this->month = $this->get('MonthTable');
		$this->allMonthLabels = $this->get('AllMonthLabels');
		$this->monthLabel = $this->get('CurrentMonthLabel');

		// Display the view
		parent::display($tpl);
	}
}