<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
?>

<div class="com_prayertt">
	<h1><?php echo $this->monthLabel ?></h1>

	<ul class="nav nav-pills">
		<?php foreach ($this->allMonthLabels as $m) { ?>
	  	<li role="presentation" class="<?php echo $m['active'] ?>">
	  		<a href="<?php echo JRoute::_('index.php?option=com_prayertt&month='.$m['key']) ?>">
	  			<?php echo $m['label'] ?>
	  		</a>
	  	</li>
	  	<?php } ?>
	</ul>
	
	<table class="com_prayertt table table-responsive table-striped">
		<thead>
			<tr>
				<th>Date</th>
				<th class="hidden-xs">Imsaak</th>
				<th>Fajr</th>
				<th>Sunrise</th>
				<th>Zohr</th>
				<th class="hidden-xs">Sunset</th>
				<th>Maghrib</th>
			</tr>
		</thead>
		<tbody>
		<?php foreach ($this->month as $day) { ?>
			<tr>
				<td><?php echo $day->Date ?></td>
				<td class="hidden-xs"><?php echo $day->Imsaak ?></td>
				<td><?php echo $day->Fajr ?></td>
				<td><?php echo $day->Sunrise ?></td>
				<td><?php echo $day->Zohr ?></td>
				<td class="hidden-xs"><?php echo $day->Sunset ?></td>
				<td><?php echo $day->Maghrib ?></td>
			</tr>
		<?php }?>
		</tbody>
	</table>
</div>