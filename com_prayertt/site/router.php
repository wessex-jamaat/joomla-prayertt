<?php
defined('_JEXEC') or die;

class PrayerTTRouter extends JComponentRouterBase
{
        public function build(&$query)
        {
                $segments = array();

                if (isset($query['month']))
                {
                        $segments[] = $query['month'];
                        unset($query['month']);
                }

                $total = count($segments);

                for ($i = 0; $i < $total; $i++)
                {
                        $segments[$i] = str_replace(':', '-', $segments[$i]);
                }

                return $segments;
        }

        public function parse(&$segments)
        {
                $total = count($segments);
                $vars = array();

                for ($i = 0; $i < $total; $i++)
                {
                        $segments[$i] = preg_replace('/-/', ':', $segments[$i], 1);
                }

                // View is always the first element of the array
                $count = count($segments);

                if ($count)
                {
                        $count--;
                        $segment = array_shift($segments);

                        $vars['month'] = $segment;
                }

                return $vars;
        }
}
